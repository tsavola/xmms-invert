# $Id: Makefile,v 1.1 2003-08-28 15:45:12 timo Exp $

libinvert.so: libinvert.c
	gcc -O2 -Wall -fomit-frame-pointer `glib-config --cflags` \
		`glib-config --libs` -shared -o $@ libinvert.c
