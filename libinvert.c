/* $Id: libinvert.c,v 1.1 2003-08-28 15:45:12 timo Exp $ */

#include <xmms/plugin.h>

static int invert(gpointer *dataptr,
                  gint length,
                  AFormat afmt,
                  gint rate,
                  gint chcnt)
{
	if (chcnt == 2) {
		gint16 *data = (gint16 *) *dataptr;
		gint len = length >> 2;
		gint i;

		for (i = 0; i < len; i++, data += 2) {
			*data = ~*data;
		}
	}

	return length;
}

EffectPlugin *get_eplugin_info(void)
{
	static EffectPlugin ep = {
		NULL,
		NULL,
		"Channel Inverter Plugin",
		NULL,
		NULL,
		NULL,
		NULL,
		invert,
		NULL
	};

	return &ep;
}
